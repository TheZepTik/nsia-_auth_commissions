@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-6">
                    <h5>Assign and Detach Permissions</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="{{Route('permissionAssign', $user->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                    <a href="{{Route('permissionDetach', $user->id)}}" class="btn btn-danger btn-sm"><i class="fa fa-minus"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <div class="row justify-content-center">
                        {!! $users->links('pagination::bootstrap-4') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
