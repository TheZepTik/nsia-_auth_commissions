@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h5>Assign Permissions</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="{{Route('permissionAssignStore', $user->id)}}" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <pre><label>User: </label><h7>{{$user->name}}</h7></pre>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Choose permissions</label>
                        </div>
                        <input type="hidden" name="permissions" :value="permissionsSelected">
                        <div class="card">
                            <div class="card-body">
                                <h2>Permissions:</h2>
                                <div class="form-check">
                                    <div class="row">
                                        @foreach($permissions as $permission)
                                            <div class="col-md-3">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="permissionsSelected" v-model="permissionsSelected"
                                                           value="{{$permission->id}}"><em>({{$permission->display_name}})</em>
                                                </label>
                                            </div>

                                        @endforeach
                                    </div>


                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary">Assign Permissions</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var app =new Vue({
            el: '#app',
            data: {
                permissionsSelected: []
            }
        })
    </script>

@endsection
