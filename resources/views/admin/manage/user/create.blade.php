@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h5>Manage Users</h5>
                </div>
                <div class="col-md-9 text-right">
                    {{--                    <a href="{{route('userCreate')}}" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create New User</a>--}}

                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <form action="{{route('userStore')}}" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Manually type password" v-if="!auto_generate">
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="auto_generate" class="form-check-input" v-model="auto_generate">
                                        Auto Generate Password
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="role_id">Register as: </label>
                                <select name="role_id" class="form-control">
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <br>
                        <input type="hidden" name="permissions" :value="permissionsSelected">
                        <div class="card">
                            <div class="card-body">
                                <h5>Permissions:</h5>
                                <div class="form-check">
                                    <div class="row">
                                        @foreach($permissions as $permission)
                                            <div class="col-md-3">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input" name="permissionsSelected" v-model="permissionsSelected"
                                                           value="{{$permission->id}}"><em>({{$permission->display_name}})</em>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <button class="btn btn-success">Create User</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                auto_generate: false,
                rolesSelected: [],
                permissionsSelected: []

            }
        });
    </script>
@endsection
