@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h5>Manage Users</h5>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <pre><label>UserName: </label>{{$user->name}}</pre>
                            <pre><label>Email: </label>{{$user->email}}</pre>
                            <div class="col-md-6">
                                <label for="roles">Roles</label>
                                @foreach($user->roles as $role)
                                    <pre>{{$role->display_name}}</pre>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="card">
                                <div class="card-body">
                                    <h5>Specific Permissions: </h5>
                                    @if($user->permissions->count() > 0)
                                        <ul>
                                            @foreach($user->permissions as $permission)
                                                <li>{{$permission->display_name}}</li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <br><em>This user has not been assign to any specific permissions</em>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
