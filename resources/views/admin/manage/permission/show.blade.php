@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h5>Permission Details</h5>
                </div>
                <div class="col-md-9 text-right">
                    <a href="" class="btn btn-primary p-b-10"><i class="fa fa-user-plus"></i>Edit Permission</a>

                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <pre><label>Name (Display Name): </label>{{$permission->display_name}}</pre>
                        </div>
                        <div class="col-md-4">
                            <pre><label>Slug: </label>{{$permission->name}}</pre>
                        </div>
                        <div class="col-md-4">
                            <pre><label>Descripion: </label>{{$permission->description}}</pre>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

