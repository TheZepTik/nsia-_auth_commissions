@extends('admin.layouts.admin')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h5>Role Details</h5>
                </div>
                <div class="col-md-9 text-right">
                    <a href="" class="btn btn-primary p-b-10"><i class="fa fa-user-plus"></i>Edit Permission</a>

                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <pre><label>Name (Display Name): </label>{{$role->display_name}}</pre>
                            <pre><label>Slug: </label>{{$role->name}}</pre>
                            <pre><label>Descripion: </label>{{$role->description}}</pre>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h2>Permissions: </h2>
                                    <ul>
                                        @foreach($role->permissions as $permission)
                                            <li>{{$permission->display_name}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

