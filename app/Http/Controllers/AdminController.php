<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laratrust\Models\LaratrustPermission;
use Laratrust\Models\LaratrustRole;


class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(['role:admin']);
    }

    public function dashboard(){
        return view('admin.dashboard');
    }

    public function userIndex(){
        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('admin.manage.user.index', compact('users'));
    }

    public function userCreate(){
        $roles = Role::all();
        $permissions = Permission::all();
        return view('admin.manage.user.create', compact('roles', 'permissions'));
    }

    public function userStore(Request $request){
        $this->validate($request, [
            'name'          => 'required|max:255',
            'email'         => 'required|email|unique:users'
        ]);

        if (!empty($request->password)){
            $password = trim($request->password);
        }
        else{
            $password = 'password';
        }

        $user = new User;
        $user->name         =$request->name;
        $user->email         =$request->email;
        $user->password         =Hash::make($password);

        $user->save();
        $user->attachRole($request->role_id);
        if($request->permissions){
            $user->syncPermissions(explode(',', $request->permissions));
        }
        return redirect()->route('userIndex');

    }

    public function usershow($id){
        $user = User::find($id);
        return view('admin.manage.user.show', compact('user'));

    }

    public function userEdit($id){
        $user = User::find($id);
        $roles = Role::all();
        $permissions = Permission::all();
        return view('admin.manage.user.edit', compact('user','roles', 'permissions'));

    }

    public function userUpdate(Request $request, $id){
        $this->validate($request, [
            'name'          => 'required|max:255',
            'email'         => 'required|email|unique:users,email,'.$id
        ]);

        $user = User::find($id);


        if ($request->dontchange == true){
            $password = $user->password;
        }
        else{
            $password = $request->password;
            $user->password   = Hash::make($password);

        }

        $user->name         =$request->name;
        $user->email         =$request->email;

        $user->save();
//        $user->detachRole($request->role_id);
//        $user->attachRole($request->role_id);
        $user->syncRoles(explode(',', $request->role_id));

        $user->detachPermission($request->permission);

        if($request->permissions){
            $user->syncPermissions(explode(',', $request->permissions));
        }

        return redirect()->route('userIndex');

    }



    public function permissionIndex(){
        $permissions = Permission::orderby('id', 'desc')->paginate(10);
        return view('admin.manage.permission.index', compact('permissions'));
    }

    public function permissionCreate(){
        return view('admin.manage.permission.create');
    }

    public function permissionStore(Request $request){

        if($request->permission_type == 'basic'){
            $this->validate($request, [
                'name'  =>'required|max:255|alphadash|unique:permissions,name',
                'display_name'   =>'required|max:255',
                'description'  =>'sometimes|max:255'
            ]);

            $permission = new Permission;
            $permission->display_name  = $request->display_name;
            $permission->name  = $request->name;
            $permission->description  = $request->description;
            $permission->save();

            return redirect()->route('permissionIndex');
        }
        elseif($request->permission_type == 'crud'){
            $this->validate($request, [
                'resource'   =>'required|min:3|max:100',

            ]);
            $crud = explode(',', $request->crud_selected);
            if(count($crud) > 0){
                foreach ($crud as $x){
                    $display_name = ucwords($x . ' ' . $request->resource);
                    $slug  = strtolower($x) . '-' . $request->resource;
                    $description  =  'Allow a user to ' . strtoupper($x) . ' a ' . ucwords($request->resource);

                    $permission = new Permission;
                    $permission->display_name  =$display_name;
                    $permission->name  = $slug;
                    $permission->description  = $description;
                    $permission->save();
                }
            }

            return redirect()->route('permissionIndex');

        }

else{
            return redirect()->route('permissionCreate')->withInput();
        }
    }

    public function permissionShow($id){
        $permission = Permission::findOrFail($id);
        return view('admin.manage.permission.show', compact('permission'));
    }

    public function roleIndex(){
        $roles = Role::orderby('id', 'desc')->paginate(10);
        return view('admin.manage.role.index', compact('roles'));
    }

    public function roleCreate(){
        $permissions = Permission::all();
        return view('admin.manage.role.create', compact('permissions'));
    }

    public function roleStore(Request $request){
        $this->validate($request, [
            'display_name'   =>'required|max:255',
            'name'  =>'required|max:255|alphadash|unique:permissions,name',
            'description'  =>'sometimes|max:255'
        ]);

        $role = new Role;
        $role->display_name  = $request->display_name;
        $role->name  = $request->name;
        $role->description  = $request->description;
        $role->save();



        if($request->permissions){
            $role->syncPermissions(explode(',', $request->permissions));
        }

        return redirect()->route('roleIndex');
    }

    public function roleShow($id){
        $role = Role::findOrFail($id);
        return view('admin.manage.role.show', compact('role'));
    }

    public function userList(){
        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('admin.assignment.permission.index', compact('users'));
    }

    public function permissionAssign($id){
        $user = User::find($id);
        $permissions = Permission::all();
        return view('admin.assignment.permission.assign', compact('permissions', 'user'));
    }

    public function permissionAssignStore(Request $request, $id){
//        dd($request->all());
        $user = User::findOrFail($id);
        $user->permissions()->sync($request->permissions);
        return redirect()->route('userList');
    }

}
