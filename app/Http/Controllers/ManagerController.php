<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManagerController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware(['role:manager']);
    }

    public function dashboard(){
        return view('manager.dashboard');
    }
}
