<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('test');
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('register', 'App\Http\Controllers\Auth\RegisterController@store')->name('dashboard');

Route::group(['middleware' => ['auth']], function() {
    Route::get('dashboard', 'App\Http\Controllers\Auth\RegisterController@index')->name('ledashboard');
});

////auth route for Admin
Route::group(['prefix' => 'admin/', 'middleware' => ['role:admin']], function(){
    Route::get('dashboard', 'App\Http\Controllers\AdminController@dashboard')->name('adminDashboard');


    Route::get('manage.user.index', 'App\Http\Controllers\AdminController@userIndex')->name('userIndex');
    Route::get('manage.user.create', 'App\Http\Controllers\AdminController@userCreate')->name('userCreate');
    Route::post('manage.user.store', 'App\Http\Controllers\AdminController@userStore')->name('userStore');
    Route::get('manage.user.show/{id}', 'App\Http\Controllers\AdminController@userShow')->name('userShow');
    Route::get('manage.user.edit/{id}', 'App\Http\Controllers\AdminController@userEdit')->name('userEdit');
    Route::post('manage.user.update/{id}', 'App\Http\Controllers\AdminController@userUpdate')->name('userUpdate');


    Route::get('manage.permission.index', 'App\Http\Controllers\AdminController@permissionIndex')->name('permissionIndex');
    Route::get('manage.permission.create', 'App\Http\Controllers\AdminController@permissionCreate')->name('permissionCreate');
    Route::post('manage.permission.store', 'App\Http\Controllers\AdminController@permissionStore')->name('permissionStore');
    Route::get('manage.permission.show/{id}', 'App\Http\Controllers\AdminController@permissionShow')->name('permissionShow');

    Route::get('manage.role.index', 'App\Http\Controllers\AdminController@roleIndex')->name('roleIndex');
    Route::get('manage.role.create', 'App\Http\Controllers\AdminController@roleCreate')->name('roleCreate');
    Route::post('manage.role.store', 'App\Http\Controllers\AdminController@roleStore')->name('roleStore');
    Route::get('manage.role.show/{id}', 'App\Http\Controllers\AdminController@roleShow')->name('roleShow');

    Route::get('assignment.permission.index', 'App\Http\Controllers\AdminController@userList')->name('userList');
    Route::get('assignment.permission.assign/{id}', 'App\Http\Controllers\AdminController@permissionAssign')->name('permissionAssign');
    Route::post('assignment.permission.store/{id}', 'App\Http\Controllers\AdminController@permissionAssignStore')->name('permissionAssignStore');
    Route::get('assignment.permission.detach/{id}', 'App\Http\Controllers\AdminController@permissionDetach')->name('permissionDetach');






});

Route::group(['prefix' => 'manager/', 'middleware' => ['role:manager']], function(){
    Route::get('dashboard', 'App\Http\Controllers\ManagerController@dashboard')->name('managerDashboard');

});


////auth route for Admin
//Route::group(['middleware' => ['auth' , 'role:admin']], function() {
//    Route::get('manage.user.index', 'App\Http\Controllers\AdminController@userIndex')->name('userIndex');
//    Route::get('manage.user.create', 'App\Http\Controllers\AdminController@userCreate')->name('userCreate');
//    Route::post('manage.user.store', 'App\Http\Controllers\AdminController@userStore')->name('userStore');
////    Route::get('/manage.user.show/{id}', 'App\Http\Controllers\AdminController@show')->whereNumber('id')->name('userShow');
//    Route::get('manage.user.show.{id}', [App\Http\Controllers\AdminController::class, 'show'])->whereNumber('id')->name('userShow');
//
//
//    Route::get('manage.permission.index', 'App\Http\Controllers\AdminController@permissionIndex')->name('permissionIndex');
//    Route::get('manage.permission.create', 'App\Http\Controllers\AdminController@permissionCreate')->name('permissionCreate');
//    Route::post('manage.permission.store', 'App\Http\Controllers\AdminController@permissionStore')->name('permissionStore');
//
//    Route::get('manage.role.index', 'App\Http\Controllers\AdminController@roleIndex')->name('roleIndex');
//    Route::get('manage.role.create', 'App\Http\Controllers\AdminController@roleCreate')->name('roleCreate');
//    Route::post('manage.role.store', 'App\Http\Controllers\AdminController@roleStore')->name('roleStore');

